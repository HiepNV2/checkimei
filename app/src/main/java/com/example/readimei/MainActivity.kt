package com.example.readimei

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.telephony.TelephonyManager
import android.widget.Button
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat


class MainActivity : AppCompatActivity() {

    lateinit var btnCheckImei: Button
    lateinit var tvImei: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnCheckImei = findViewById(R.id.btnCheckImei)
        tvImei = findViewById(R.id.tvImei)

        btnCheckImei.setOnClickListener {
            checkPermissions()
        }
    }

    private fun checkPermissions() {
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.READ_PHONE_STATE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermission.launch(android.Manifest.permission.READ_PHONE_STATE)
        } else {
            getImeiNumber()
        }
    }

    private val requestPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                getImeiNumber()
            }
        }

    private fun getImeiNumber() {
        try {
//            // For android Q
//            var deviceId = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//                Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
//            } else {
//                val mTelephony = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
//                if (mTelephony.deviceId != null) {
//                    mTelephony.deviceId
//                } else {
//                    Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
//                }
//            }
//            tvImei.text = deviceId.toString()
            val mTelephony = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            if (mTelephony?.deviceId != null) {
                tvImei.text = mTelephony?.deviceId.toString()
            }
        } catch (e: SecurityException) {
            tvImei.text = e.message
        }
    }
}
